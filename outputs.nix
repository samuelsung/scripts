{ self
, flake-utils
, nixpkgs
}:

let
  inherit (flake-utils.lib) eachDefaultSystem;

  packages = import ./packages;
in
eachDefaultSystem (system:
  let
    pkgs = import nixpkgs {
      inherit system;
      overlays = [ packages ];
    };
    inherit (pkgs) mkShell;
  in
  {
    devShell = mkShell {
      buildInputs = with pkgs; [ shellspec ];
    };
  }
) // {
  overlay = packages;
}
