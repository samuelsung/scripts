#shellcheck shell=sh

Describe 'general.sh'
  Include lib/general.sh

  Describe 'gen_rand_name'
    It 'generate 10 characters'
      count_chars() {
        get_rand_name | wc --chars
      }
      When call count_chars
      The output should equal 10
    End

    It 'prints random string every time'
      When call get_rand_name
      The output should not equal "$(get_rand_name)"
    End
  End

  Describe 'gen_ssh_key_json'
    gen_ssh_key() {
      echo "private_key_${1}_${3}" > "$2"
      echo "public_key_${1}_${3}" > "${2}.pub"
    }

    It 'generate a json with public key and private key'
      When call gen_ssh_key_json comment passphrase
      The output should equal '{"private":"private_key_comment_passphrase\n","public":"public_key_comment_passphrase\n"}'
    End
  End
End
