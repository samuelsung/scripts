#!/bin/sh

# Read password from tty
# returns the password got
# exit when fails
read_password() {
  prompt=$1 # Message for prompting the user to input the password

  # always read from the tty even when redirected:
  exec </dev/tty || exit # || exit only needed for bash

  # save current tty settings:
  tty_settings=$(stty -g) || exit

  # schedule restore of the settings on exit of that subshell
  # or on receiving SIGINT or SIGTERM:
  trap 'stty "$tty_settings"' EXIT INT TERM

  # disable terminal local echo
  stty -echo || exit

  # prompt on tty
  printf '%s' "$prompt" >/dev/tty

  # read password as one line, record exit status
  read -r local_password || {
    ret=$?
    echo >/dev/tty && exit "$ret"
  }
  echo >/dev/tty

  echo "$local_password"
}

# Read password from tty with confirmation
# returns the password got
# exit when fails
read_password_with_confirm() {
  while [ -z ${passphrase+x} ] || [ -z ${passphrase_again+x} ] || [ "$passphrase" != "$passphrase_again" ]; do {
    if [ -n "${passphrase+x}" ]; then
      printf "Passphrases do not match.  Try again.\n"
    fi

    passphrase=$(read_password "Enter passphrase (empty for no passphrase): ")

    if [ "${passphrase}" = "" ]; then
      break
    fi

    passphrase_again=$(read_password "Enter same passphrase again: ")
  }; done

  echo "$passphrase"
}

# generate a random 10 char. string
get_rand_name() {
  tr </dev/urandom --delete -c 'a-zA-Z' | head -c 10
}

to_json_string() {
  jq <"$1" -sR
}

gen_ssh_key() {
  comment=$1
  location=$2
  passphrase=$3
  yes y | ssh-keygen -t ed25519 -N "$passphrase" -q -f "$location" -C "$comment"
}

gen_ssh_key_json() {
  comment=$1
  passphrase=$2
  location="/tmp/$(get_rand_name)"
  gen_ssh_key_json_json="$(mkfifo "$location" && mkfifo "$location.pub" && ( (
    cat <<EOF
{"private":$(to_json_string "$location"),"public":$(to_json_string "$location.pub")}
EOF
    rm "$location"
    rm "$location.pub"
  ) &) && (gen_ssh_key "$comment" "$location" "$passphrase" >/dev/null))"

  [ -n "$gen_ssh_key_json_json" ] && echo "$gen_ssh_key_json_json"
}

gen_json_host_secret_file() {
  target=$1     # target sops file
  input_json=$2 # json content

  file_name=$(basename "$target")
  dir_name=$(dirname "$target")

  tmp_location="/tmp/$(get_rand_name)-${file_name}"

  [ -d "${dir_name}" ] || mkdir -p "${dir_name}"

  mkfifo "$tmp_location" && ( (
    echo "$input_json" >"$tmp_location"
    rm "$tmp_location"
  ) &) && (
    sops -e "$tmp_location" >"$target"
  )
}
