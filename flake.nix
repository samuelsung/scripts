{
  description = "Personal scripts for various purposes";

  inputs = {
    flake-utils.url = "github:numtide/flake-utils";

    nixpkgs.url = "nixpkgs/nixos-21.11";
  };

  outputs = args: import ./outputs.nix args;
}
