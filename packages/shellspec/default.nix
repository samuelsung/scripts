{ lib, stdenv, fetchFromGitHub }:

stdenv.mkDerivation rec {
  pname = "shellspec";
  version = "0.28.1";

  src = fetchFromGitHub {
    owner = "shellspec";
    repo = "shellspec";
    rev = version;
    sha256 = "1ib5qp29f2fmivwnv6hq35qhvdxz42xgjlkvy0i3qn758riyqf46";
  };

  buildPhase = "make test";

  installPhase = ''
    mkdir -p $out
    PREFIX="$out" make install
  '';

  meta = with lib; {
    homepage = "https://github.com/shellspec/shellspec";
    description = "ShellSpec is a full-featured BDD unit testing framework for dash, bash, ksh, zsh and all POSIX shells.";
    license = licenses.mit;
    platforms = platforms.unix;
  };
}
