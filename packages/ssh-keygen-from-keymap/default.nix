{ pkgs, lib, stdenv }:

stdenv.mkDerivation rec {
  name = "ssh-keygen-from-keymap";

  nativeBuildInputs = with pkgs; [
    jq
    coreutils
    openssh
    sops
  ];

  src = ../../src;

  buildPhase = "";

  installPhase = ''
    mkdir -p $out/bin
    cp -r $src $out/src
    ln -s $out/src/ssh-keygen-from-keymap $out/bin
    chmod +x $out/bin/ssh-keygen-from-keymap
  '';

  meta = with lib; {
    homepage = "https://gitlab.com/samuelsung/scripts";
    description = "Script for generating ssh keys from a json key-mapping";
    license = licenses.mit;
    platforms = platforms.unix;
  };
}
